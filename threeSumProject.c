#include<stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>

int main()
{
	FILE *fp;
	char *line = NULL;
	size_t len = 0;
	ssize_t read;
	int l = 0;
	double x, y, z, xC, yC, zC;
	pid_t proc, PROC, id;

	x = clock();	// Parent Clock
	if((id = fork()) == 0) // child process
	{
		xC = clock();	// Child Clock

		fp = fopen("num.dat", "r");
		if(fp == NULL)
		exit(EXIT_FAILURE);
	
		while((read = getline(&line, &len, fp)) != -1)// read/displays each line in the file
		{
			l++;
			// printf("Retrieved line of length %u :\n", read);
			// printf("%s", line);
		}
		printf("Num of lines in file %d\n", l);

		free(line);
		fclose(fp);
		//exit(EXIT_FAILURE);

		char ch;
		int b, c, d, h, num, sum , a[l], i = 0;
		// tok = strtok(line, \n);
		char *tok;

		fp = fopen("num.dat", "r");

		while((read = getline(&line, &len, fp)) != -1) 
		{
			tok = strtok(line, "\n"); // breaks the line breaks in the read file
			// printf("%s\n", tok);
			//ch = fgetc(fp);
			num = atoi(tok); // converts num into an integal
			a[i] = num;
			//printf("%d\n", a[i]); 
			i++;
		}

		for(b = 0; b < l; b++)	// (O)n loop
		{
			for (c = b + 1; c < l; c++) // (O)n^2 loop
			{
				for (d = c + 1; d < l; d++) // (O)n^3 loop
			{
				{
					sum = a[b] + a[c] + a[d];
					if (sum == 23) {
					printf("The sum of %d + %d + %d = %d\n",  a[b], a[c], a[d], sum);
				}
			}
		}
	}

	fclose(fp);

	yC = clock();
	zC = (yC - xC) / CLOCKS_PER_SEC;
	printf("Runtime for child function is: %lf\n", zC);
	}
	y = clock();
	z = (y - x) / CLOCKS_PER_SEC;
	printf("Runtime for program is: %lf\n", z);
}